<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Form</title>
  <link href="https://fonts.googleapis.com/css?family=Archivo+Black" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Hammersmith+One" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Baloo+Tammudu" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet">
  <link rel="stylesheet" href="../../../resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../../resources/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../../resources/css/normalize.css">
  <link rel="stylesheet" href="../../../resources/css/style.css">
</head>
<body background="../../../resources/images/background.jpg" style="background-attachment: fixed; background-repeat: no-repeat; background-size: 100%">

<div class="container">

  <h1>OOP Mark Grading system</h1>

  <form action="process.php" method="post">
    <div class="student_info">
      <h2>Student Information</h2>

      <label class="control-label" for="stuId">Student Id:</label>
      <input type="text" name="studentId" class="form-control" placeholder="Input your ID" autofocus required>
      <br><br>

      <label class="control-label" for="name">Name:</label>
      <input type="text" name="name" class="form-control" placeholder="Type Your Name" autofocus required>
      <br><br>

      <label class="control-label" for="date">DOB:</label>
      <input type="date" name="dob" autofocus required>

      <br><br>
      <label class="control-label">Gender</label>
      <br><br>
      <input type="radio" name="gender" value="Male" >
      <label for="male">Male</label>

      <input type="radio" name="gender" value="Female">
      <label for="female">Female</label>

      <br><br>
    </div>
    <div class="mark_info">
      <h2>Mark Information</h2>

      <label class="control-label" for="bn">Bangla:</label>
      <input type="text" name="banglaMark" class="form-control-two" autofocus required>
      <br><br>
      <label class="control-label" for="en">English:</label>
      <input type="text" name="englishMark" class="form-control-two" autofocus required>
      <br><br>
      <label class="control-label" for="math">Math:</label>
      <input type="text" name="mathMark" class="form-control-two" autofocus required>
      <br><br>
      <label class="control-label" for="ict">ICT:</label>
      <input type="text" name="ictMark" class="form-control-two" autofocus required>
      <br><br><br>

    </div>
      <input class="btn btn-primary" type="submit" name="submit" value="Submit Information">


    <p>&nbsp;</p>  <p>&nbsp;</p>
  </form>

</div>

</body>
</html>
