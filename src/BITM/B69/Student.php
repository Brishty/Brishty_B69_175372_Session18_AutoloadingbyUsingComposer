<?php

namespace App;



class Student extends Person
{
    protected $studentID, $banglaMark, $englishMark, $mathMark, $ICTMark;


    public function setData($postArray){

        if(array_key_exists("studentId",$postArray))
              $this->studentID = $postArray["studentId"];
        if(array_key_exists("name",$postArray))
              $this->name  = $postArray["name"];
        if(array_key_exists("dob",$postArray))
              $this->dob = $postArray["dob"];
        if(array_key_exists("gender",$postArray))
              $this->gender = $postArray["gender"];

        if(array_key_exists("banglaMark",$postArray))
            $this->banglaMark = $postArray["banglaMark"];
        
        if(array_key_exists("englishMark",$postArray))
            $this->englishMark = $postArray["englishMark"];

        if(array_key_exists("mathMark",$postArray))
            $this->mathMark = $postArray["mathMark"];
        
        if(array_key_exists("ictMark",$postArray))
            $this->ICTMark = $postArray["ictMark"];

    }// end of setData()



    public function markToGrade($mark){

        switch($mark){
            case ($mark>79) :
                $grade = "A+";
                break;

            case ($mark>74) :
                $grade = "A";
                break;

            case ($mark>69) :
                $grade = "A-";
                break;

            case ($mark>64) :
                $grade = "B+";
                break;
            case ($mark>59) :
                $grade = "B";
                break;

            case ($mark>54) :
                $grade = "B-";
                break;

            case ($mark>49) :
                $grade = "C";
                break;

            case ($mark >= 40) :
                $grade = "D";
                break;

            default:
                $grade = "F";
        }
        return $grade;
    }


    public function findFinalResult(){

        if($this->banglaMark<40 || $this->englishMark<40 || $this->mathMark<40 || $this->ICTMark<40)
              return "Fail";
        else  return "Pass";
        
    }

    public function getData(){

        echo "Student ID: ". $this->studentID . "<br>";
        echo "Student Name: ". $this->name . "<br>";
        echo "Student DOB: ". $this->dob . "<br>";
        echo "Student Gender: ". $this->gender . "<hr>";
        echo "Bangla Mark: ". $this->banglaMark . "  Grade:" . $this->markToGrade($this->banglaMark). "<br>";
        echo "English Mark: ". $this->englishMark . "  Grade:" . $this->markToGrade($this->englishMark). "<br>";
        echo "Math Mark: ". $this->mathMark . "  Grade:" . $this->markToGrade($this->mathMark). "<br>";
        echo "ICT Mark: ". $this->ICTMark ."  Grade:" . $this->markToGrade($this->ICTMark). "<hr>";


        $str =  "<h1> Overall Result Status: ". $this->findFinalResult(). "</h1>";

        echo $str . "<hr>";


    }

}// end of Student Class